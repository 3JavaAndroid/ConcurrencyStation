package Model;

public interface INotifiable {
	void receiveNotify();
}
